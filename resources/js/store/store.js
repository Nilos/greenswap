import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        loginStatus: ''
    },
    mutations:{
        loginStatus(state, loginStatus){
            state.loginStatus = loginStatus;
        }
    },
    getters: {
        loginStatus: state => state.loginStatus
    },
});