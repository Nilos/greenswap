import ArticlesListe from "./views/ArticlesListe";

require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import { store } from './store/store'

Vue.use(VueRouter);

import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue);
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
Vue.component('v-icon', Icon)

import App from './views/App'
import Hello from './views/Hello'
import Home from './views/Home'
import AnnoncesListe from './views/AnnoncesListe'
import ChatApp from './components/messenger/ChatApp'

import axios from 'axios';
import AnnonceSolo from "./views/AnnonceSolo";
import DonsListe from "./views/DonsListe";

Vue.prototype.$http =  axios;

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/',
            name: 'app',
            component: Home
        },
        {
            path: '/hello',
            name: 'hello',
            component: Hello,
        },
        {
            path: '/annonces',
            name: 'annonces.liste',
            component: AnnoncesListe,
        },
        {
            path: '/annonces/:id',
            name: 'annonces.solo',
            component: AnnonceSolo,
        },
        {
            path: '/messenger',
            name: 'messenger.index',
            component: ChatApp,
        },
        {
            path: '/articles',
            name: 'articles.index',
            component: ArticlesListe,
        },
        {
            path: '/dons',
            name: 'dons.index',
            component: DonsListe,
        },
        { path: '*', redirect: '/' },
    ],
});

export default {
    components: {
        'v-icon': Icon
    }
}

const app = new Vue({
    el: '#app',
    store,
    components: { App },
    router,
});