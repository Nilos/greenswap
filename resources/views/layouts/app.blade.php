<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>GreenSwap</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.js"></script>
    <script src="../../node_modules/vue-awesome/dist/vue-awesome.js"></script>

    <style lang="scss">
        @import "../../../node_modules/bootstrap/scss/bootstrap.scss";

        .bg-blue{
            background-color: #a7e0ce;
        }
        .bg-black{
            background-color: black;
        }
        .b-black{
            border: 1px solid black;
        }
        .bg-img-login{
            background-image: url('../../images/3cactus.jpg');
            background-size:cover;
            background-position: center;
        }
        .container-size{
            position:absolute;
            width: 100%;
            height: 100%;
        }
        .form-control {
            display: block;
            width: 100%;
            height: calc(1.5em + .75rem + 2px);
            padding: .375rem .75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 3px solid black;
            border-radius: 0;
            transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }
    </style>

</head>
<body>
<div id="app">
    <b-container fluid class="bg-img-login container-size">
        <!--Nav-bar-->
        <b-navbar toggleable="lg" type="dark" variant="" class="bg-blue fixed-top">
            <b-navbar-brand href="/" variant="text-dark" class="ml-4 pl-1"><b class="color-black">GreenSwap</b></b-navbar-brand>

            <b-navbar-toggle target="nav-collapse"></b-navbar-toggle>

            <b-collapse id="nav-collapse" is-nav>
                <!-- Right aligned nav items -->
                <b-navbar-nav class="ml-auto mr-4">
                    <b-nav-item href="/annonces" class="mr-4 ml-4"><span class="color-black">Rechercher</span></b-nav-item>

                    <b-nav-item-dropdown right class="ml-4">
                        <!-- Using 'button-content' slot -->
                        <template slot="button-content" class="mr-5"><span class="color-black mr-2"><v-icon name="lock" scale="1"/></span></template>
                        <b-dropdown-item href="/register">S'inscrire</b-dropdown-item>
                        <b-dropdown-item href="/login">Se connecter</b-dropdown-item>
                    </b-nav-item-dropdown>
                </b-navbar-nav>
            </b-collapse>
        </b-navbar>

        <main class="py-4">
            @yield('content')
        </main>
    </b-container>
</div>
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
