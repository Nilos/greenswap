<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
@if(Auth::user()->isAdmin())
    <li><a href='{{ backpack_url('annonce') }}'><i class='fa fa-pagelines'></i> <span>Annonces</span></a></li>
    <li><a href='{{ backpack_url('typeAnnonce') }}'><i class='fa fa-tag'></i> <span>Type Annonces</span></a></li>
    <li><a href='{{ backpack_url('user') }}'><i class='fa fa-user-circle'></i> <span>Utilisateurs</span></a></li>
    <li><a href='{{ backpack_url('don') }}'><i class='fa fa-pagelines'></i> <span>Dons</span></a></li>
    <li><a href='{{ backpack_url('article') }}'><i class='fa fa-pagelines'></i> <span>Articles</span></a></li>
    <li><a href='{{ backpack_url('plante') }}'><i class='fa fa-pagelines'></i> <span>Plantes</span></a></li>
    <li><a href='{{ backpack_url('typePlante') }}'><i class='fa fa-pagelines'></i> <span>Types Plantes</span></a></li>
    <li><a href='{{ backpack_url('proprietePlante') }}'><i class='fa fa-pagelines'></i> <span>Propriete Plantes</span></a></li>
    <li><a href='{{ backpack_url('typeEnvironnement') }}'><i class='fa fa-tag'></i> <span>Type Environnements</span></a></li>
@else
    <li><a href='{{ backpack_url('annonce') }}'><i class='fa fa-pagelines'></i> <span>Mes annonces</span></a></li>
    <li><a href='/messenger'><i class='fa fa-tag'></i> <span>Mes messages</span></a></li>
    <li><a href='/dons'><i class='fa fa-tag'></i> <span>Action éco-responsable</span></a></li>
    <li><a href='/articles'><i class='fa fa-tag'></i> <span>Bibliothèque</span></a></li>
@endif

<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>