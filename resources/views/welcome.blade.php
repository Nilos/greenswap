<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>GreenSwap</title>
    </head>
    <body>
    <div>
        <h1>Test Model</h1>

        <h2>Annonce</h2>
        <?php
            $annonce = \App\Annonce::all()->first();
        ?>
        {{ $annonce->lieu }} <br>
        {{ $annonce->url_image }} <br>
        {{ $annonce->description }} <br>
        {{ $annonce->user->prenom }} <br>
        {{ $annonce->typeAnnonce->type }}

        <h2>Plante</h2>
        <?php
        $plante = \App\Plante::all()->first();
        ?>
        {{ $plante->nom }} <br>
        {{ $plante->famille }} <br>
        {{ $plante->typePlante->type }} <br>
        {{ $plante->typeEnvironnement->type }} <br>
        {{ $plante->proprietePlantes }} <br>

        <h2>Propriété plante</h2>
        <?php
        $propriete_plante = \App\ProprietePlante::all()->first();
        ?>
        {{ $propriete_plante->propriete }} <br>
        {{ $propriete_plante->url_icon }} <br>
        {{ $propriete_plante->plantes }}

        <h2>Tag</h2>
        <?php
        $tag = \App\Tag::all()->first();
        ?>
        {{ $tag->tag }}

        <h2>Type annonce</h2>
        <?php
        $type_annonce = \App\TypeAnnonce::all()->first();
        ?>
        {{ $type_annonce->type }} <br>
        {{ $type_annonce->annonces }}

        <h2>Type environnement</h2>
        <?php
        $type_environnement = \App\TypeEnvironnement::all()->first();
        ?>
        {{ $type_environnement->type }} <br>
        {{ $type_environnement->url_icon }} <br>
        {{ $type_environnement->plantes }}

        <h2>Type plante</h2>
        <?php
        $type_plante = \App\TypePlante::all()->first();
        ?>
        {{ $type_plante->type }} <br>
        {{ $type_plante->url_icon }} <br>
        {{ $type_plante->plantes }}
    </div>
    </body>
</html>
