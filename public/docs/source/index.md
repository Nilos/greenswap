---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_1cef42261c0ab0c66df83a09904231e5 -->
## All announces

Used to display all announces (10 per page)
You can use get request with "page" parameter to change the page.

> Example request:

```bash
curl -X GET -G "http://localhost/api/annonces" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/annonces");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "id": 1,
    "description": "Et sed non debitis veniam recusandae. Exercitationem enim est non commodi repellat eveniet aut. Et quis est impedit architecto inventore in ipsa. Expedita modi rerum similique explicabo.",
    "url_image": "https:\/\/lorempixel.com\/640\/480\/?73029",
    "lieu": "95926 Mitchell Views Apt. 059\nNew Vanessachester, MA 63125",
    "created_at": {
        "date": "2019-03-06 20:03:01.000000",
        "timezone_type": 3,
        "timezone": "UTC"
    },
    "updated_at": {
        "date": "2019-03-06 20:03:01.000000",
        "timezone_type": 3,
        "timezone": "UTC"
    }
}
```

### HTTP Request
`GET api/annonces`


<!-- END_1cef42261c0ab0c66df83a09904231e5 -->

<!-- START_34363b74c33e7fc716cb0616b5764e31 -->
## Display an announce

Used to display an announce by his id

> Example request:

```bash
curl -X GET -G "http://localhost/api/annonces/{annonce}" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/annonces/{annonce}");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "id": 1,
    "description": "Et sed non debitis veniam recusandae. Exercitationem enim est non commodi repellat eveniet aut. Et quis est impedit architecto inventore in ipsa. Expedita modi rerum similique explicabo.",
    "url_image": "https:\/\/lorempixel.com\/640\/480\/?73029",
    "lieu": "95926 Mitchell Views Apt. 059\nNew Vanessachester, MA 63125",
    "created_at": {
        "date": "2019-03-06 20:03:01.000000",
        "timezone_type": 3,
        "timezone": "UTC"
    },
    "updated_at": {
        "date": "2019-03-06 20:03:01.000000",
        "timezone_type": 3,
        "timezone": "UTC"
    }
}
```

### HTTP Request
`GET api/annonces/{annonce}`


<!-- END_34363b74c33e7fc716cb0616b5764e31 -->

<!-- START_60f9fba320c9f3f743447e195ea3ea28 -->
## Create an announce

> Example request:

```bash
curl -X POST "http://localhost/api/annonces" \
    -H "Authorization: Bearer: {token}" \
    -H "Content-Type: application/json" \
    -d '{"description":"Je propose des plantes gratuitement. Vous pouvez les r\u00e9cup\u00e9rer en me contactant.","url_image":"plantes.png","lieu":"Paris 15e"}'

```

```javascript
const url = new URL("http://localhost/api/annonces");

let headers = {
    "Authorization": "Bearer: {token}",
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "description": "Je propose des plantes gratuitement. Vous pouvez les r\u00e9cup\u00e9rer en me contactant.",
    "url_image": "plantes.png",
    "lieu": "Paris 15e"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "id": 1,
    "description": "Et sed non debitis veniam recusandae. Exercitationem enim est non commodi repellat eveniet aut. Et quis est impedit architecto inventore in ipsa. Expedita modi rerum similique explicabo.",
    "url_image": "https:\/\/lorempixel.com\/640\/480\/?73029",
    "lieu": "95926 Mitchell Views Apt. 059\nNew Vanessachester, MA 63125",
    "created_at": {
        "date": "2019-03-06 20:03:01.000000",
        "timezone_type": 3,
        "timezone": "UTC"
    },
    "updated_at": {
        "date": "2019-03-06 20:03:01.000000",
        "timezone_type": 3,
        "timezone": "UTC"
    }
}
```

### HTTP Request
`POST api/annonces`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    description | string |  required  | The description of the post.
    url_image | string |  required  | The image of the post.
    lieu | string |  required  | The place of the announce.

<!-- END_60f9fba320c9f3f743447e195ea3ea28 -->

<!-- START_bd61b6013368bc9ccae28c6c9ad453e5 -->
## Update an announce

> Example request:

```bash
curl -X PUT "http://localhost/api/annonces/{annonce}" \
    -H "Authorization: Bearer: {token}" \
    -H "Content-Type: application/json" \
    -d '{"description":"Je propose des plantes gratuitement. Vous pouvez les r\u00e9cup\u00e9rer en me contactant.","url_image":"plantes.png","lieu":"Paris 15e"}'

```

```javascript
const url = new URL("http://localhost/api/annonces/{annonce}");

let headers = {
    "Authorization": "Bearer: {token}",
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "description": "Je propose des plantes gratuitement. Vous pouvez les r\u00e9cup\u00e9rer en me contactant.",
    "url_image": "plantes.png",
    "lieu": "Paris 15e"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "id": 1,
    "description": "Et sed non debitis veniam recusandae. Exercitationem enim est non commodi repellat eveniet aut. Et quis est impedit architecto inventore in ipsa. Expedita modi rerum similique explicabo.",
    "url_image": "https:\/\/lorempixel.com\/640\/480\/?73029",
    "lieu": "95926 Mitchell Views Apt. 059\nNew Vanessachester, MA 63125",
    "created_at": {
        "date": "2019-03-06 20:03:01.000000",
        "timezone_type": 3,
        "timezone": "UTC"
    },
    "updated_at": {
        "date": "2019-03-06 20:03:01.000000",
        "timezone_type": 3,
        "timezone": "UTC"
    }
}
```

### HTTP Request
`PUT api/annonces/{annonce}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    description | string |  required  | The description of the post.
    url_image | string |  required  | The image of the post.
    lieu | string |  required  | The place of the announce.

<!-- END_bd61b6013368bc9ccae28c6c9ad453e5 -->

<!-- START_f0ef423c68f10c54b2f07f341c2d0da1 -->
## Delete an announce

Used to delete an announce

> Example request:

```bash
curl -X DELETE "http://localhost/api/annonces/{annonce}" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/annonces/{annonce}");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/annonces/{annonce}`


<!-- END_f0ef423c68f10c54b2f07f341c2d0da1 -->

<!-- START_3607627eaec55efbd8227ef9b5db62f2 -->
## All plantes

Used to display all plantes

> Example request:

```bash
curl -X GET -G "http://localhost/api/plantes" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/plantes");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response:

```json
null
```

### HTTP Request
`GET api/plantes`


<!-- END_3607627eaec55efbd8227ef9b5db62f2 -->

<!-- START_0bab6a498f48d7847ec4c041b6dfcb59 -->
## Display a plante

Used to display a plante by his id

> Example request:

```bash
curl -X GET -G "http://localhost/api/plantes/{plante}" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/plantes/{plante}");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response:

```json
null
```

### HTTP Request
`GET api/plantes/{plante}`


<!-- END_0bab6a498f48d7847ec4c041b6dfcb59 -->

<!-- START_4a2bd732249112e23d0cc2a1c212d7b3 -->
## Create a plante

Used to create a plante

> Example request:

```bash
curl -X POST "http://localhost/api/plantes" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/plantes");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/plantes`


<!-- END_4a2bd732249112e23d0cc2a1c212d7b3 -->

<!-- START_97d616e77141bd87405b24dd76ec4ae5 -->
## Update a plante

Used to update a plante

> Example request:

```bash
curl -X PUT "http://localhost/api/plantes/{plante}" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/plantes/{plante}");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT api/plantes/{plante}`


<!-- END_97d616e77141bd87405b24dd76ec4ae5 -->

<!-- START_b803804cfcc97572eef07d17727ae6a5 -->
## Delete a plante

Used to delete a plante

> Example request:

```bash
curl -X DELETE "http://localhost/api/plantes/{id}" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/plantes/{id}");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/plantes/{id}`


<!-- END_b803804cfcc97572eef07d17727ae6a5 -->

<!-- START_17b627521918d1d342e6b05d8da1b869 -->
## All propriete plante

Used to display all propriete plante

> Example request:

```bash
curl -X GET -G "http://localhost/api/proprietes" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/proprietes");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response:

```json
null
```

### HTTP Request
`GET api/proprietes`


<!-- END_17b627521918d1d342e6b05d8da1b869 -->

<!-- START_fedc098d7d16aa5779313b752efee823 -->
## Display a propriete plante

Used to display a propriete plante by his id

> Example request:

```bash
curl -X GET -G "http://localhost/api/proprietes/{proprietePlante}" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/proprietes/{proprietePlante}");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response:

```json
null
```

### HTTP Request
`GET api/proprietes/{proprietePlante}`


<!-- END_fedc098d7d16aa5779313b752efee823 -->

<!-- START_46f4cce84ff6b88693d738f3fea502d9 -->
## Create a propriete plante

Used to create a propriete plante

> Example request:

```bash
curl -X POST "http://localhost/api/proprietes" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/proprietes");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/proprietes`


<!-- END_46f4cce84ff6b88693d738f3fea502d9 -->

<!-- START_79dfad0650c14e6742295a2c27a9647e -->
## Update a propriete plante

Used to update a propriete plante

> Example request:

```bash
curl -X PUT "http://localhost/api/proprietes/{proprietePlante}" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/proprietes/{proprietePlante}");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT api/proprietes/{proprietePlante}`


<!-- END_79dfad0650c14e6742295a2c27a9647e -->

<!-- START_d1ea39f9e9fd797a981a3affd26cc697 -->
## Delete a propriete plante

Used to delete a propriete plante

> Example request:

```bash
curl -X DELETE "http://localhost/api/proprietes/{id}" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/proprietes/{id}");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/proprietes/{id}`


<!-- END_d1ea39f9e9fd797a981a3affd26cc697 -->

<!-- START_3927d9858a98b64816c08b1fb902df4b -->
## All type plante

Used to display all type plante

> Example request:

```bash
curl -X GET -G "http://localhost/api/types" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/types");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response:

```json
null
```

### HTTP Request
`GET api/types`


<!-- END_3927d9858a98b64816c08b1fb902df4b -->

<!-- START_e5907c12f6c3df7b4df6b03dbedb5695 -->
## Display a type plante

Used to display a type plante by his id

> Example request:

```bash
curl -X GET -G "http://localhost/api/types/{typePlante}" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/types/{typePlante}");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response:

```json
null
```

### HTTP Request
`GET api/types/{typePlante}`


<!-- END_e5907c12f6c3df7b4df6b03dbedb5695 -->

<!-- START_68d51ffb9d8ffaf4457a17232b149007 -->
## Create a type plante

Used to create a type plante

> Example request:

```bash
curl -X POST "http://localhost/api/types" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/types");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/types`


<!-- END_68d51ffb9d8ffaf4457a17232b149007 -->

<!-- START_c8a383ad7a32be5ea1aa5c41b2bdc997 -->
## Update a type plante

Used to update a type plante

> Example request:

```bash
curl -X PUT "http://localhost/api/types/{typePlante}" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/types/{typePlante}");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT api/types/{typePlante}`


<!-- END_c8a383ad7a32be5ea1aa5c41b2bdc997 -->

<!-- START_a8e6bc24162d7d4df51cea9b731bb0f6 -->
## Delete a type plante

Used to delete a type plante

> Example request:

```bash
curl -X DELETE "http://localhost/api/types/{id}" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/types/{id}");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/types/{id}`


<!-- END_a8e6bc24162d7d4df51cea9b731bb0f6 -->

<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## Connection

> Example request:

```bash
curl -X POST "http://localhost/api/login" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/login");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/login`


<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->

<!-- START_d7b7952e7fdddc07c978c9bdaf757acf -->
## Register

> Example request:

```bash
curl -X POST "http://localhost/api/register" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/register");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/register`


<!-- END_d7b7952e7fdddc07c978c9bdaf757acf -->

<!-- START_a69bb9a29be86907a2edb0c13ea1f993 -->
## User details

> Example request:

```bash
curl -X POST "http://localhost/api/details" \
    -H "Authorization: Bearer: {token}"
```

```javascript
const url = new URL("http://localhost/api/details");

let headers = {
    "Authorization": "Bearer: {token}",
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/details`


<!-- END_a69bb9a29be86907a2edb0c13ea1f993 -->


