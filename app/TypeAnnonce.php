<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeAnnonce extends Model
{
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'type_annonce';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
    ];

    /**
     * Get the Annonces for the TypeAnnonce.
     */
    public function annonces()
    {
        return $this->hasMany('App\Annonce');
    }
}
