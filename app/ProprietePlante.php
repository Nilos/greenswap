<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProprietePlante extends Model
{
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'propriete_plante';

    // /**
    //  * The attributes that are mass assignable.
    //  *
    //  * @var array
    //  */
    // protected $fillable = [
    //     'propriete', 'url_icon',
    // ];
    protected $guarded =[];
    /**
     * The Plantes that belong to the ProprietePlante.
     */
    public function plantes()
    {
        return $this->belongsToMany('App\Plante');
    }
}
