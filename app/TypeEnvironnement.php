<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeEnvironnement extends Model
{
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'type_environnement';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
    ];

    /**
     * Get the Plantes that owns the TypeEnvironnement.
     */
    public function plantes()
    {
        return $this->hasMany('App\Plante');
    }
}
