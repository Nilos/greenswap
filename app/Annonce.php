<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Annonce extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'annonce';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'url_image', 'lieu'
    ];

    /**
     * Get the User that owns the Annonce.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the TypeAnnonce that owns the Annonce.
     */
    public function typeAnnonce()
    {
        return $this->belongsTo('App\TypeAnnonce');
    }

    /**
     * The Plantes that belong to the Annonce.
     */
    public function plantes()
    {
        return $this->belongsToMany('App\Plante');
    }

    /**
     * The users favoris that belong to the Annonce.
     */
    public function favoris()
    {
        return $this->belongsToMany('App\User', 'favoris');
    }

    public function isUserFavoris(){
        if(auth('api')){
            if($this->favoris->contains(auth('api')->id())){
                return 1;
            }
        }
        return 0;
    }
}
