<?php

namespace App\Observers;

use App\Models\Annonce;

class AnnonceObserver
{
    /**
     * Handle the annonce "created" event.
     *
     * @param  App\Models\Annonce  $annonce
     * @return void
     */
    public function created(Annonce $annonce)
    {
        //
        if(!backpack_user()->isAdmin()) {
            $annonce->user_id = backpack_user()->id;
            $annonce->save();
        }
    }

    /**
     * Handle the annonce "updated" event.
     *
     * @param  App\Models\Annonce  $annonce
     * @return void
     */
    public function updated(Annonce $annonce)
    {
        //
    }

    /**
     * Handle the annonce "deleted" event.
     *
     * @param  App\Models\Annonce  $annonce
     * @return void
     */
    public function deleted(Annonce $annonce)
    {
        //
    }

    /**
     * Handle the annonce "restored" event.
     *
     * @param  App\Models\Annonce  $annonce
     * @return void
     */
    public function restored(Annonce $annonce)
    {
        //
    }

    /**
     * Handle the annonce "force deleted" event.
     *
     * @param  App\Models\Annonce  $annonce
     * @return void
     */
    public function forceDeleted(Annonce $annonce)
    {
        //
    }
}
