<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ProprietePlanteRequest as StoreRequest;
use App\Http\Requests\ProprietePlanteRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class ProprietePlanteCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ProprietePlanteCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ProprietePlante');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/proprietePlante');
        $this->crud->setEntityNameStrings('propriété de plante', 'Propriétés de plantes');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        //tableau Read
        $this->crud->addColumn(['name' => 'propriete','label' => 'Propriete','type' => 'text']);
        $this->crud->addColumn(['name' => 'url_icon','label' => 'Icone','type' => 'image']);

        //Field pour l'update
        $this->crud->addField(['name' => 'propriete','label' => 'Propriete','type' => 'text']);
        $this->crud->addField([    // Image
            'name' => 'url_icon',
            'label' => 'Image',
            'type' => 'image',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
        ]);

        // add asterisk for fields that are required in ProprietePlanteRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
