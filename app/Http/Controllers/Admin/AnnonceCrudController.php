<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AnnonceRequest as StoreRequest;
use App\Http\Requests\AnnonceRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class AnnonceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AnnonceCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Annonce');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/annonce');
        $this->crud->setEntityNameStrings('annonce', 'annonces');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        //$users = \App\Models\User::find(1)->user;

        //tableau Read
        $this->crud->addColumn(['name' => 'titre','label' => 'Titre','type' => 'text']);
        $this->crud->addColumn(['name' => 'description', 'type' => 'text', 'label' => 'Description']);
        $this->crud->addColumn(['name' => 'url_image','label' => 'Image','type' => 'image']);
        $this->crud->addColumn(['name' => 'lieu','label' => 'Lieu','type' => 'text']);

        //Field pour l'update
        $this->crud->addField(['name' => 'titre', 'type' => 'text', 'label' => 'Titre']);
        $this->crud->addField(['name' => 'description', 'type' => 'text', 'label' => 'Description']);
        //$this->crud->addField(['name' => 'url_image','label' => 'url_image','type' => 'text']);
        $this->crud->addField(['name' => 'lieu','label' => 'Lieu','type' => 'text']);


        if(backpack_user()->isAdmin()) {
            $this->crud->addField([
                'label' => "Utilisateur",
                'type' => 'select',
                'name' => 'user_id', // the db column for the foreign key
                'entity' => 'user', // the method that defines the relationship in your Model
                'attribute' => 'full_name', // foreign key attribute that is shown to user
                'model' => "App\Models\User", // foreign key model
                'pivot' => false, // on create&update, do you need to add/delete pivot table entries?
            ]);
        }

        $this->crud->addField([
            'label' => "Type d'annonce",
            'type' => 'select',
            'name' => 'type_annonce_id', // the db column for the foreign key
            'entity' => 'typeAnnonce', // the method that defines the relationship in your Model
            'attribute' => 'type', // foreign key attribute that is shown to user
            'model' => "App\Models\TypeAnnonce", // foreign key model
            'pivot' => false, // on create&update, do you need to add/delete pivot table entries?
        ]);

        $this->crud->addField([    // Select2Multiple = n-n
            'label' => 'Plantes',
            'type' => 'select2_multiple',
            'name' => 'plantes',
            'entity' => 'plantes',
            'attribute' => 'nom',
            'model' => "App\Models\Plante",
            'pivot' => true,
        ]);

        $this->crud->addField([    // Image
            'name' => 'url_image',
            'label' => 'Image',
            'type' => 'image',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
        ]);

        // add asterisk for fields that are required in AnnonceRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        // on recupere seulement les annonces de l'utilisateur connecté s'il n'est pas admin.
        if(!backpack_user()->isAdmin()){
            $this->crud->addClause('where', 'user_id', '=', backpack_user()->id);
        }

    }

    public function store(StoreRequest $request)
    {
        //$users = \App\Models\User::get();
        //var_dump($request->user_id);
        //die;

        $user = \App\Models\User::where('id', $request->user_id)->first();

        // var_dump($user->name);
        // die;
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'user_id');
    }
}
