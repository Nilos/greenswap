<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DonRequest as StoreRequest;
use App\Http\Requests\DonRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class DonCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class DonCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Don');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/don');
        $this->crud->setEntityNameStrings('don', 'dons');

        //tableau Read
        $this->crud->addColumn(['name' => 'titre', 'label' => 'Titre', 'type' => 'text']);
        $this->crud->addColumn(['name' => 'description','label' => 'Description','type' => 'summernote']);
        $this->crud->addColumn(['name' => 'url_lien','label' => 'Site internet','type' => 'text']);

        //Field pour l'update
        $this->crud->addField(['name' => 'titre', 'label' => 'Titre', 'type' => 'text']);
        $this->crud->addField(['name' => 'description','label' => 'Description','type' => 'summernote']);
        $this->crud->addField(['name' => 'url_lien','label' => 'Site internet','type' => 'text']);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        // add asterisk for fields that are required in DonRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
