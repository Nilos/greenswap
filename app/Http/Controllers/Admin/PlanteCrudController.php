<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PlanteRequest as StoreRequest;
use App\Http\Requests\PlanteRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class PlanteCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PlanteCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Plante');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/plante');
        $this->crud->setEntityNameStrings('plante', 'plantes');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        //tableau Read
        $this->crud->addColumn(['name' => 'nom', 'type' => 'text', 'label' => 'nom']);
        $this->crud->addColumn(['name' => 'famille','label' => 'famille','type' => 'text']);
        $this->crud->addColumn(['name' => 'created_at','label' => 'Crée le','type' => 'date']);
        $this->crud->addColumn([
            'name' => 'type_environnement_id',
            'label' => 'Type d\'environnement',
            'type' => 'select',
            'entity' => 'typeEnvironnement', // the method that defines the relationship in your Model
            'attribute' => 'type', // foreign key attribute that is shown to user
            'model' => "App\Models\TypeEnvironnement", // foreign key model
        ]);
        $this->crud->addColumn([
            'name' => 'type_plante_id',
            'label' => 'Type de plante',
            'type' => 'select',
            'entity' => 'typePlante', // the method that defines the relationship in your Model
            'attribute' => 'type', // foreign key attribute that is shown to user
            'model' => "App\Models\TypePlante", // foreign key model
        ]);

        //Field pour l'update
        $this->crud->addField(['name' => 'nom', 'type' => 'text', 'label' => 'nom']);
        $this->crud->addField(['name' => 'famille','label' => 'famille','type' => 'text']);

        $this->crud->addField([
            'label' => "Type Plante",
            'type' => 'select',
            'name' => 'type_plante_id', // the db column for the foreign key
            'entity' => 'typePlante', // the method that defines the relationship in your Model
            'attribute' => 'type', // foreign key attribute that is shown to user
            'model' => "App\Models\TypePlante", // foreign key model
            'pivot' => false, // on create&update, do you need to add/delete pivot table entries?
        ]);

        $this->crud->addField([
            'label' => "Type Environnement",
            'type' => 'select',
            'name' => 'type_environnement_id', // the db column for the foreign key
            'entity' => 'typeEnvironnement', // the method that defines the relationship in your Model
            'attribute' => 'type', // foreign key attribute that is shown to user
            'model' => "App\Models\TypeEnvironnement", // foreign key model
            'pivot' => false, // on create&update, do you need to add/delete pivot table entries?
        ]);

        // add asterisk for fields that are required in PlanteRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here

        $typeEnvironnement = \App\Models\TypeEnvironnement::where('id', $request->type_environnement_id)->first();
        $typePlante = \App\Models\TypePlante::where('id', $request->type_plante_id)->first();

        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function typeEnvironnement()
    {
        return $this->belongsToMany('App\Models\TypeEnvironnement', 'id');
    }

}
