<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\TypeAnnonceRequest as StoreRequest;
use App\Http\Requests\TypeAnnonceRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class TypeAnnonceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class TypeAnnonceCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\TypeAnnonce');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/typeAnnonce');
        $this->crud->setEntityNameStrings('type d\'annonce', 'Types d\'annonces');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        //tableau Read
        $this->crud->addColumn(['name' => 'type', 'type' => 'text', 'label' => 'Type']);

        //Field pour l'update
        $this->crud->addField(['name' => 'type', 'type' => 'text', 'label' => 'Type']);

        // add asterisk for fields that are required in TypeAnnonceRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
