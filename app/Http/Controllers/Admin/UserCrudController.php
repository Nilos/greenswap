<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\UserRequest as StoreRequest;
use App\Http\Requests\UserRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class UserCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\User');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/user');
        $this->crud->setEntityNameStrings('utilisateur', 'utilisateurs');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        //tableau Read
        $this->crud->addColumn(['name' => 'name', 'type' => 'text', 'label' => 'Nom']);
        $this->crud->addColumn(['name' => 'email','label' => 'Adresse Email','type' => 'email']);
        $this->crud->addColumn(['name' => 'password','label' => 'Mot de passe','type' => 'text']);
        $this->crud->addColumn(['name' => 'date_de_naissance','label' => 'Date de naissance','type' => 'date']);
        $this->crud->addColumn(['name' => 'adresse','label' => 'Adresse','type' => 'address']);
        $this->crud->addColumn(['name' => 'ville','label' => 'Ville','type' => 'text']);
        $this->crud->addColumn(['name' => 'pays','label' => 'Pays','type' => 'text']);
        $this->crud->addColumn(['name' => 'created_at','label' => 'Date de création','type' => 'date']);
        $this->crud->addColumn(['name' => 'updated_at','label' => 'Mise à jour','type' => 'date']);

        //Field pour l'update
        $this->crud->addField(['name' => 'name', 'type' => 'text', 'label' => 'Nom']);
        $this->crud->addField(['name' => 'email','label' => 'Adresse Email','type' => 'email']);
        $this->crud->addField(['name' => 'password','label' => 'Mot de passe','type' => 'password']);
        $this->crud->addField(['name' => 'date_de_naissance','label' => 'Date de naissance','type' => 'date']);
        $this->crud->addField(['name' => 'adresse','label' => 'Adresse','type' => 'address']);
        $this->crud->addField(['name' => 'ville','label' => 'Ville','type' => 'text']);
        $this->crud->addField(['name' => 'pays','label' => 'Pays','type' => 'text']);
        $this->crud->addField(['name' => 'created_at','label' => 'Date de création','type' => 'date']);
        $this->crud->addField(['name' => 'updated_at','label' => 'Mise à jour','type' => 'date']);

        // add asterisk for fields that are required in UserRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        $request['password'] = bcrypt($request['password']);
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $request['password'] = bcrypt($request['password']);
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
