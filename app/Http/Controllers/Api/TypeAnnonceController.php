<?php

namespace App\Http\Controllers\Api;

use App\Annonce;
use App\Http\Resources\AnnonceResource;
use App\Http\Resources\TypeAnnonceResource;
use App\TypeAnnonce;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TypeAnnonceController extends Controller
{
    /**
     * All type annonces
     *
     * Used to display all types annonces
     *
     * @response {
     *   "id": 1,
     *   "type": "donner"
     *   }
     * }
     */
    public function index()
    {
        $typesAnnonces = TypeAnnonce::all();

        return TypeAnnonceResource::collection($typesAnnonces);
    }
}
