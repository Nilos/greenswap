<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\DonResource;
use App\Models\Don;
use App\Plante;
use App\Http\Resources\PlanteResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DonController extends Controller
{
    /**
     * All dons
     *
     * Used to display all dons
     */
    public function index()
    {
        //return new \App\Http\Resources\CommonCollectionResource(\App\Annonce::with(['plantes', 'user', 'typeAnnonce'])->get());
        return DonResource::collection(Don::paginate(10));
    }

    /**
     * Display a don
     *
     * Used to display a don by his id
     */
    public function show(Don $don)
    {
        return new DonResource($don);
    }
}
