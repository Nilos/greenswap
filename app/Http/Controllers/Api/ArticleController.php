<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ArticleResource;
use App\Http\Resources\DonResource;
use App\Models\Article;
use App\Models\Don;
use App\Plante;
use App\Http\Resources\PlanteResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    /**
     * All articles
     *
     * Used to display all article
     */
    public function index()
    {
        //return new \App\Http\Resources\CommonCollectionResource(\App\Annonce::with(['plantes', 'user', 'typeAnnonce'])->get());
        return ArticleResource::collection(Article::paginate(10));
    }

    /**
     * Display a article
     *
     * Used to display a article by his id
     */
    public function show(Don $article)
    {
        return new ArticleResource($article);
    }
}
