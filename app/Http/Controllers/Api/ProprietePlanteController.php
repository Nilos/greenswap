<?php

namespace App\Http\Controllers\Api;

use App\ProprietePlante;
use App\Http\Resources\ProprietePlanteResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProprietePlanteController extends Controller
{
    /**
     * All propriete plante
     *
     * Used to display all propriete plante
     */
    public function index()
    {
        //return new \App\Http\Resources\CommonCollectionResource(\App\Annonce::with(['plantes', 'user', 'typeAnnonce'])->get());
        return ProprietePlanteResource::collection(ProprietePlante::with(['plantes'])->paginate(1));
    }

    /**
     * Display a propriete plante
     *
     * Used to display a propriete plante by his id
     */
    public function show(ProprietePlante $proprietePlante)
    {
        $proprietePlante->load(['user', 'typePlante']);
        return new ProprietePlanteResource($proprietePlante);
    }

    /**
     * Create a propriete plante
     *
     * Used to create a propriete plante
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $proprietePlante = ProprietePlante::create($data);

        return response()->json($proprietePlante, 201);
    }

    /**
     * Update a propriete plante
     *
     * Used to update a propriete plante
     */
    public function update(Request $request, ProprietePlante $proprietePlante)
    {
        $proprietePlante->update($request->all());

        return response()->json($proprietePlante, 200);
    }

    /**
     * Delete a propriete plante
     *
     * Used to delete a propriete plante
     */
    public function delete($id)
    {
        ProprietePlante::find($id)->delete();

        return response()->json(null, 204);
    }
}
