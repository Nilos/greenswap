<?php

namespace App\Http\Controllers\Api;

use App\Annonce;
use App\Http\Resources\AnnonceResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AnnonceController extends Controller
{
    /**
     * All announces
     *
     * Used to display all announces (10 per page)
     * You can use get request with "page" parameter to change the page.
     *
     * @response {
     *   "id": 1,
     *   "description": "Et sed non debitis veniam recusandae. Exercitationem enim est non commodi repellat eveniet aut. Et quis est impedit architecto inventore in ipsa. Expedita modi rerum similique explicabo.",
     *   "url_image": "https://lorempixel.com/640/480/?73029",
     *   "lieu": "95926 Mitchell Views Apt. 059\nNew Vanessachester, MA 63125",
     *   "created_at": {
     *   "date": "2019-03-06 20:03:01.000000",
     *   "timezone_type": 3,
     *   "timezone": "UTC"
     *   },
     *   "updated_at": {
     *   "date": "2019-03-06 20:03:01.000000",
     *  "timezone_type": 3,
     *   "timezone": "UTC"
     *   }
     * }
     */
    public function index(Request $request)
    {
        $annonces = Annonce::with(['plantes', 'user', 'typeAnnonce']);

        if($request->input('type_plante_id')){
            $annonces = $annonces->whereHas( 'plantes', function($q) use($request){
                $q->where('plante_id', $request->input('type_plante_id'));
            });
        }

        if($request->input('type_annonce_id')){
            $annonces = $annonces->where('type_annonce_id', $request->input('type_annonce_id'));
        }

        if($request->input('ville')){
            $annonces = $annonces->where('lieu', 'LIKE', '%'.$request->input('ville').'%');
        }

        if($request->input('favoris')){
            $annonces = $annonces->whereHas('favoris', function ($query) {
                $query->where('user_id', auth('api')->id());
            });
        }

        $annonces = $annonces->paginate(10);

        return AnnonceResource::collection($annonces);
    }

    /**
     * Display an announce
     *
     * Used to display an announce by his id
     *
     * @response {
     *   "id": 1,
     *   "description": "Et sed non debitis veniam recusandae. Exercitationem enim est non commodi repellat eveniet aut. Et quis est impedit architecto inventore in ipsa. Expedita modi rerum similique explicabo.",
     *   "url_image": "https://lorempixel.com/640/480/?73029",
     *   "lieu": "95926 Mitchell Views Apt. 059\nNew Vanessachester, MA 63125",
     *   "created_at": {
     *   "date": "2019-03-06 20:03:01.000000",
     *   "timezone_type": 3,
     *   "timezone": "UTC"
     *   },
     *   "updated_at": {
     *   "date": "2019-03-06 20:03:01.000000",
     *  "timezone_type": 3,
     *   "timezone": "UTC"
     *   }
     * }
     */
    public function show(Annonce $annonce)
    {
        $annonce->load(['plantes', 'user', 'typeAnnonce', 'favoris']);
        return new AnnonceResource($annonce);
    }

    /**
     * Create an announce
     *
     * @bodyParam description string required The description of the post. Example: Je propose des plantes gratuitement. Vous pouvez les récupérer en me contactant.
     * @bodyParam url_image string required The image of the post. Example: plantes.png
     * @bodyParam lieu string required The place of the announce. Example: Paris 15e
     *
     * Used to create an announce
     *
     * @response {
     *   "id": 1,
     *   "description": "Et sed non debitis veniam recusandae. Exercitationem enim est non commodi repellat eveniet aut. Et quis est impedit architecto inventore in ipsa. Expedita modi rerum similique explicabo.",
     *   "url_image": "https://lorempixel.com/640/480/?73029",
     *   "lieu": "95926 Mitchell Views Apt. 059\nNew Vanessachester, MA 63125",
     *   "created_at": {
     *   "date": "2019-03-06 20:03:01.000000",
     *   "timezone_type": 3,
     *   "timezone": "UTC"
     *   },
     *   "updated_at": {
     *   "date": "2019-03-06 20:03:01.000000",
     *  "timezone_type": 3,
     *   "timezone": "UTC"
     *   }
     * }
     */
    public function store(Request $request)
    {

        $data = $request->all();
        $annonce = Annonce::create($data);

        return response()->json($annonce, 201);
    }

    /**
     * Update an announce
     *
     * @bodyParam description string required The description of the post. Example: Je propose des plantes gratuitement. Vous pouvez les récupérer en me contactant.
     * @bodyParam url_image string required The image of the post. Example: plantes.png
     * @bodyParam lieu string required The place of the announce. Example: Paris 15e
     *
     * Used to update an announce
     *
     * @response {
     *   "id": 1,
     *   "description": "Et sed non debitis veniam recusandae. Exercitationem enim est non commodi repellat eveniet aut. Et quis est impedit architecto inventore in ipsa. Expedita modi rerum similique explicabo.",
     *   "url_image": "https://lorempixel.com/640/480/?73029",
     *   "lieu": "95926 Mitchell Views Apt. 059\nNew Vanessachester, MA 63125",
     *   "created_at": {
     *   "date": "2019-03-06 20:03:01.000000",
     *   "timezone_type": 3,
     *   "timezone": "UTC"
     *   },
     *   "updated_at": {
     *   "date": "2019-03-06 20:03:01.000000",
     *  "timezone_type": 3,
     *   "timezone": "UTC"
     *   }
     * }
     *
     */
    public function update(Request $request, Annonce $annonce)
    {
        $annonce->update($request->except('favoris'));


        if($request->input('favoris')){
            if($request->input('favoris') == 1){
                if (! $annonce->favoris->contains(auth('api')->id())) {
                    $annonce->favoris()->attach(auth('api')->id());
                }
            }
        }
        else{
            $annonce->favoris()->detach(auth('api')->id());
        }
        $annonce->save();

        $annonce->load('favoris');
        return response()->json(new AnnonceResource($annonce), 200);
    }

    /**
     * Delete an announce
     *
     * Used to delete an announce
     */
    public function delete(Annonce $annonce)
    {
        $annonce->delete();

        return response()->json(null, 204);
    }
}
