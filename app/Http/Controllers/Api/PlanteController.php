<?php

namespace App\Http\Controllers\Api;

use App\Plante;
use App\Http\Resources\PlanteResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlanteController extends Controller
{
    /**
     * All plantes
     *
     * Used to display all plantes
     */
    public function index()
    {
        //return new \App\Http\Resources\CommonCollectionResource(\App\Annonce::with(['plantes', 'user', 'typeAnnonce'])->get());
        return PlanteResource::collection(Plante::with(['typePlante'])->get());
    }

    /**
     * Display a plante
     *
     * Used to display a plante by his id
     */
    public function show(Plante $plante)
    {
        $plante->load(['user', 'typePlante']);
        return new PlanteResource($plante);
    }

    /**
     * Create a plante
     *
     * Used to create a plante
     */
    public function store(Request $request)
    {

        $data = $request->all();
        $plante = Plante::create($data);

        return response()->json($plante, 201);
    }

    /**
     * Update a plante
     *
     * Used to update a plante
     */
    public function update(Request $request, Plante $plante)
    {
        $plante->update($request->all());

        return response()->json($plante, 200);
    }

    /**
     * Delete a plante
     *
     * Used to delete a plante
     */
    public function delete($id)
    {
        Plante::find($id)->delete();

        return response()->json(null, 204);
    }
}
