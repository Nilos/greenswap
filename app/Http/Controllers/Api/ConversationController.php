<?php
namespace App\Http\Controllers\API;
use App\Events\NewMessage;
use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
class ConversationController extends Controller
{

    public function getMessagesFor($id){

        //marque tous les messages du contact selectionné en "vu"
        Message::where('from', $id)->where('to', Auth::id())->update(['read' => true]);

        $messages = Message::where(function ($q) use($id) {
            $q->where('from', Auth::id());
            $q->where('to', $id) ;
        })->orWhere(function ($q) use($id){
            $q->where('from', $id);
            $q->where('to', Auth::id()) ;
        })->orderBy('id', 'DESC')
        ->get();

        return response()->json($messages);
    }

    public function send(Request $request){
        $message = Message::create([
            'from' => Auth::id(),
            'to' => $request->contact_id,
            'text' => $request->text,
        ]);

        broadcast(new NewMessage($message));

        return response()->json($message);
    }

}