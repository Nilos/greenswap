<?php

namespace App\Http\Controllers\Api;

use App\TypePlante;
use App\Http\Resources\TypePlanteResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TypePlanteController extends Controller
{
    /**
     * All type plante
     *
     * Used to display all type plante
     */
    public function index()
    {
        //return new \App\Http\Resources\CommonCollectionResource(\App\Annonce::with(['plantes', 'user', 'typeAnnonce'])->get());
        return TypePlanteResource::collection(TypePlante::with(['plantes'])->paginate(1));
    }

    /**
     * Display a type plante
     *
     * Used to display a type plante by his id
     */
    public function show(TypePlante $typePlante)
    {
      return new TypePlanteResource($typePlante);
        $typePlante->load(['type', 'plantes', 'url_icon']);
    }

    /**
     * Create a type plante
     *
     * Used to create a type plante
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $typePlante = TypePlante::create($data);

        return response()->json($typePlante, 201);
    }

    /**
     * Update a type plante
     *
     * Used to update a type plante
     */
    public function update(Request $request, TypePlante $typePlante)
    {
        $typePlante->update($request->all());

        return response()->json($typePlante, 200);
    }

    /**
     * Delete a type plante
     *
     * Used to delete a type plante
     */
    public function delete($id)
    {
        TypePlante::find($id)->delete();

        return response()->json(null, 204);
    }
}
