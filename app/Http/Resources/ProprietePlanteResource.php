<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProprietePlanteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'propriete' => $this->propriete,
            'url_icon' => $this->url_icon,
            'plantes' => PlanteResource::collection($this->whenLoaded('plantes')),
        ];
    }
}
