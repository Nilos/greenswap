<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AnnonceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'titre' => $this->titre,
            'description' => $this->description,
            'url_image' => $this->url_image,
            'lieu' => $this->lieu,
            'isUserFavoris' => $this->isUserFavoris(),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'plantes' => PlanteResource::collection($this->whenLoaded('plantes')), // une annonce a PLUSIEURS plantes
            'user' => new UserResource($this->whenLoaded('user')), // une annonce appartient à UN SEUL user
            'typeAnnonce' => new TypeAnnonceResource($this->whenLoaded('typeAnnonce')),
        ];
    }
}
