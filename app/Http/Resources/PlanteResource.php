<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PlanteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nom' => $this->nom,
            'famille' => $this->famille,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'typeEnvironnement' => new TypeEnvironnementResource($this->whenLoaded('typeEnvironnement')), // une annonce appartient à UN SEUL user
            'typePlante' => new TypePlanteResource($this->whenLoaded('typePlante')),
        ];
    }
}
