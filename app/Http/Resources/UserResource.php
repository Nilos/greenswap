<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'prenom' => $this->prenom,
            'pseudo' => $this->pseudo,
            'adresse' => $this->adresse,
            'ville' => $this->ville,
            'pays' => $this->pays,
            'date_de_naissance' => $this->date_de_naissance,
            'users' => $this->date_de_naissance,
            'annonces' => AnnonceResource::collection($this->whenLoaded('annonces')),
            'userRole' => new UserRoleResource($this->whenLoaded('userRole')),
        ];
    }
}
