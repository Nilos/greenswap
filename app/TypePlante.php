<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypePlante extends Model
{
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'type_plante';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'propriete',
    ];

    /**
     * Get the Plantes that owns the TypePlante.
     */
    public function plantes()
    {
        return $this->hasMany('App\Plante');
    }
}
