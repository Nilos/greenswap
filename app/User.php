<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'prenom',  'pseudo', 'adresse', 'ville', 'pays', 'date_de_naissance'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the UserRole that owns the User.
     */
    public function userRole()
    {
        return $this->belongsTo('App\UserRole');
    }

    /**
     * Get the annonce for the user.
     */
    public function annonces()
    {
        return $this->hasMany('App\Annonce');
    }

    /**
     * The annonce favoris that belong to the User.
     */
    public function favoris()
    {
        return $this->belongsToMany('App\Annonce', 'favoris');
    }

    public function isUserFavoris(){
        if(auth('api')){
            if($this->favoris->contains(auth('api')->id())){
                return 1;
            }
        }
        return 0;
    }

    public function isAdmin(){
        return $this->user_role_id == 1;
    }
}
