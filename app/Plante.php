<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Plante extends Model
{
  use CrudTrait;
    /**
     *
     *
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'plante';
    //
    // /**
    //  * The attributes that are mass assignable.
    //  *
    //  * @var array
    //  */
    // protected $fillable = [
    //     'nom', 'famille',
    // ];

    protected $guarded = [];
    /**
     * Get the TypeEnvironnement that owns the Plante.
     */
    public function typeEnvironnement()
    {
        return $this->belongsTo('App\TypeEnvironnement');
    }

    /**
     * Get the TypePlante that owns the Plante.
     */
    public function typePlante()
    {
        return $this->belongsTo('App\TypePlante');
    }

    /**
     * The ProprietePlantes that belong to the Plante.
     */
    public function proprietePlantes()
    {
        return $this->belongsToMany('App\ProprietePlante');
    }

    /**
     * The Annonces that belong to the Plante.
     */
    public function annonces()
    {
        return $this->belongsToMany('App\Annonce');
    }
}
