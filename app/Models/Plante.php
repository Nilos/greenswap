<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Plante extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'plante';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
         'nom', 'famille', 'type_environnement_id', 'type_plante_id',
     ];

    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function annonce()
    {
        return $this->hasOne('App\Models\Annonce','user_id');
    }

    public function proprietePlante()
    {
        return $this->hasOne('App\Models\ProprietePlante','user_id');
    }

    /**
     * Get the TypeEnvironnement that owns the Plante.
     */
    public function typeEnvironnement()
    {
        return $this->belongsTo('App\TypeEnvironnement');
    }

    /**
     * Get the TypePlante that owns the Plante.
     */
    public function typePlante()
    {
        return $this->belongsTo('App\TypePlante');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
