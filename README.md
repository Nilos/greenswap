﻿Etape d'installation :

**Je n'ai pas fait les étapes d'installation en direct donc si vous avez un souci dites le moi (j'ai peut être oublié une étape)**

1) Choisir un répertoire d'installation
`cd <repertoire>`

2) Cloner le répertoire
```
git clone https://gitlab.com/Nilos/greenswap.git
cd greenswap
```

3) Composer (si pas installé : https://getcomposer.org/) :
`composer install`

4) Créer la base de données (dans phpmyadmin par exemple)

5a) Configurer le fichier .env avec ses identifiants (utiliser le fichier .env.example comme base)
> Pour moi par exemple
>     DB_DATABASE=greenswap
>     DB_USERNAME=root
>     DB_PASSWORD=

5b) Ajouter dans le .env (déjà présent. A modifier avec ces valeurs) :
> PUSHER_APP_ID=751510
> PUSHER_APP_KEY=714720797ae72682f064
> PUSHER_APP_SECRET=514885c3d99fd4fb19f1
> PUSHER_APP_CLUSTER=eu
> MIX_PUSHER_APP_KEY=714720797ae72682f064
> MIX_PUSHER_APP_CLUSTER=eu


6) Insérer la structure de la base de données :
`php artisan migrate`

7) Générer la clé Laravel
`php artisan key:generate`

8) Générer du contenu pour la bdd :
```
composer dump-autoload
php artisan db:seed
```

9a) Installation de passport laravel :
```
composer require laravel/passport
php artisan migrate
```

9b) php artisan passport:install --force

10) Install de Backpack
```
composer require backpack/crud "^3.5"
php artisan backpack:base:install
php artisan backpack:crud:install
```

11) Install de Elfinder
```
composer require barryvdh/laravel-elfinder
```

Remember to publish the assets after each update (or add the command to your post-update-cmd in composer.json)
```
php artisan elfinder:publish
```

12) Lancer le serveur
`php artisan serve`

13) Dans une autre fenetre de commande, aller dans le dossier du projet puis faire :
```
npm install
npm run watch
```

14) Aller sur localhost:port
