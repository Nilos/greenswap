<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('auth/logout', 'Auth\LoginController@logout');

Route::get('admin/login', function () {
    return Redirect::to('/login');
});

Auth::routes();

Route::get('/{catchall?}', 'MainController@index')->where('catchall', '^(?!api).*$');