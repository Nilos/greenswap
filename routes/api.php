<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Annonce
Route::get('annonces', 'Api\AnnonceController@index');
Route::get('annonces/{annonce}', 'Api\AnnonceController@show');
Route::post('annonces', 'Api\AnnonceController@store');
Route::put('annonces/{annonce}', 'Api\AnnonceController@update');
Route::delete('annonces/{annonce}', 'Api\AnnonceController@delete');

//Plante
Route::get('plantes', 'Api\PlanteController@index');
Route::get('plantes/{plante}', 'Api\PlanteController@show');
Route::post('plantes', 'Api\PlanteController@store');
Route::put('plantes/{plante}', 'Api\PlanteController@update');
Route::delete('plantes/{id}', 'Api\PlanteController@delete');

//Don
Route::get('dons', 'Api\DonController@index');
Route::get('dons/{don}', 'Api\DonController@show');

//Article
Route::get('articles', 'Api\ArticleController@index');
Route::get('articles/{article}', 'Api\ArticleController@show');

//Propriete Plante
Route::get('proprietes', 'Api\ProprietePlanteController@index');
Route::get('proprietes/{proprietePlante}', 'Api\ProprietePlanteController@show');
Route::post('proprietes', 'Api\ProprietePlanteController@store');
Route::put('proprietes/{proprietePlante}', 'Api\ProprietePlanteController@update');
Route::delete('proprietes/{id}', 'Api\ProprietePlanteController@delete');

//Type Plante
Route::get('types', 'Api\TypePlanteController@index');
Route::get('types/{typePlante}', 'Api\TypePlanteController@show');
Route::post('types', 'Api\TypePlanteController@store');
Route::put('types/{typePlante}', 'Api\TypePlanteController@update');
Route::delete('types/{id}', 'Api\TypePlanteController@delete');

//type annonce
Route::get('typesAnnonces', 'Api\TypeAnnonceController@index');

//test api
Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'API\UserController@details');
    Route::get('users', 'Api\UserController@index');
    Route::get('conversation/{id}', 'Api\ConversationController@getMessagesFor');
    Route::post('conversation/send', 'Api\ConversationController@send');
});
