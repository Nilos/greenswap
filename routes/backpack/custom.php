<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'user'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'user')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    // CRUD resources and other admin routes
    CRUD::resource('user', 'UserCrudController');
}); // this should be the absolute last line of this file


Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'typeEnvironnement'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'typeEnvironnement')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    // CRUD resources and other admin routes
    CRUD::resource('typeEnvironnement', 'TypeEnvironnementCrudController');
}); // this should be the absolute last line of this file

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'plante'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'plante')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    // CRUD resources and other admin routes
    CRUD::resource('plante', 'PlanteCrudController');
}); // this should be the absolute last line of this file

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'typePlante'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'typePlante')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    // CRUD resources and other admin routes
    CRUD::resource('typePlante', 'TypePlanteCrudController');
}); // this should be the absolute last line of this file

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'proprietePlante'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'proprietePlante')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    // CRUD resources and other admin routes
    CRUD::resource('proprietePlante', 'ProprietePlanteCrudController');
}); // this should be the absolute last line of this file


Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'annonce'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'annonce')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    // CRUD resources and other admin routes
    CRUD::resource('annonce', 'AnnonceCrudController');
}); // this should be the absolute last line of this file

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'typeAnnonce'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'typeAnnonce')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    // CRUD resources and other admin routes
    CRUD::resource('typeAnnonce', 'TypeAnnonceCrudController');
}); // this should be the absolute last line of this file

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'article'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'article')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    // CRUD resources and other admin routes
    CRUD::resource('article', 'ArticleCrudController');
}); // this should be the absolute last line of this file


Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'don'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'don')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    // CRUD resources and other admin routes
    CRUD::resource('don', 'DonCrudController');
}); // this should be the absolute last line of this file
