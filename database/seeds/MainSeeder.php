<?php

use Illuminate\Database\Seeder;

class MainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\User::class)->create();
        factory(App\User::class)->create(['email' => 'user@gmail.com']);

        factory(App\ProprietePlante::class)->create(['propriete' => 'medicinal']);
        factory(App\ProprietePlante::class)->create(['propriete' => 'aromathique']);
        factory(App\ProprietePlante::class)->create(['propriete' => 'comestible']);
        factory(App\ProprietePlante::class)->create(['propriete' => 'purifiante']);
        factory(App\ProprietePlante::class)->create(['propriete' => 'beaute']);

        factory(App\TypeAnnonce::class)->create(['type' => 'echanger']);
        factory(App\TypeAnnonce::class)->create(['type' => 'donner']);
        factory(App\TypeAnnonce::class)->create(['type' => 'sauver']);
        factory(App\TypeAnnonce::class)->create(['type' => 'non disponible']);
        factory(App\TypeAnnonce::class)->create(['type' => 'en culture']);

        factory(App\TypeEnvironnement::class)->create(['type' => 'indoor']);
        factory(App\TypeEnvironnement::class)->create(['type' => 'outdoor']);

        factory(App\TypePlante::class)->create(['type' => 'bouture']);
        factory(App\TypePlante::class)->create(['type' => 'plante']);
        factory(App\TypePlante::class)->create(['type' => 'fleur']);
        factory(App\TypePlante::class)->create(['type' => 'arbre']);
        factory(App\TypePlante::class)->create(['type' => 'arbre fruitier']);
        factory(App\TypePlante::class)->create(['type' => 'autre']);

        factory(App\UserRole::class)->create(['role' => 'admin']);
        factory(App\UserRole::class)->create(['role' => 'user']);

        factory(App\Annonce::class)->create();
        factory(App\Plante::class)->create([
            'nom' => 'Autre',
            'famille' => 'Autre'
        ]);

        factory(App\Tag::class)->create();

        factory(App\Message::class)->create();

        DB::table('annonce_plante')->insert([
            'created_at' => now(),
            'updated_at' => now(),
            'plante_id' => 1,
            'annonce_id' => 1,
        ]);

        DB::table('plante_propriete_plante')->insert([
            'created_at' => now(),
            'updated_at' => now(),
            'plante_id' => 1,
            'propriete_plante_id' => 1,
        ]);


    }
}
