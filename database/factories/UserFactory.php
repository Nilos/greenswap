<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => 'pignot',
        'email' => "pignotnil@gmail.com",
        'email_verified_at' => now(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'prenom' => 'nil',
        'pseudo' => 'nilos',
        'adresse' => '45 rue de Clichy 92110',
        'ville' => 'Paris',
        'pays' => 'France',
        'date_de_naissance' => '1994-06-06',
        'user_role_id' => 1,
    ];
});

$factory->define(App\Message::class, function (Faker $faker) {
    return [
        'from' => 1,
        'to' => 2,
        'text' => $faker->sentence,
    ];
});
