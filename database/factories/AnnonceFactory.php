<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Annonce::class, function (Faker $faker) {
    return [
        'description' => $faker->text,
        'titre' => $faker->text,
        'url_image' => $faker->imageUrl($width = 640, $height = 480),
        'lieu' => $faker->address,
        'created_at' => now(),
        'updated_at' => now(),
        'user_id' => 1,
        'type_annonce_id' => 1,
    ];
});
