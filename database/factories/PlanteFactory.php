<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Plante::class, function (Faker $faker) {
    return [
        'nom' => $faker->name,
        'famille' => $faker->name,
        'created_at' => now(),
        'updated_at' => now(),
        'type_plante_id' => 1,
        'type_environnement_id' => 1,
    ];
});
