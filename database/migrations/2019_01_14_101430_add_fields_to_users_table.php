<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('prenom');
            $table->string('pseudo');
            $table->string('adresse');
            $table->string('ville');
            $table->string('pays');
            $table->date('date_de_naissance');

            $table->integer('user_role_id');
            $table->foreign('user_role_id')->references('id')->on('users_roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
