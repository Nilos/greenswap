<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plante', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->string('famille');
            $table->timestamps();

            $table->integer('type_environnement_id');
            $table->foreign('type_environnement_id')->references('id')->on('type_environnement');

            $table->integer('type_plante_id');
            $table->foreign('type_plante_id')->references('id')->on('type_plante');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plante');
    }
}
