<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnoncePlanteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annonce_plante', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('plante_id');
            $table->integer('annonce_id');

            $table->foreign('plante_id')->references('id')->on('plante');
            $table->foreign('annonce_id')->references('id')->on('annonce');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annonce_plante');
    }
}
