<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanteProprietePlanteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plante_propriete_plante', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('plante_id');
            $table->integer('propriete_plante_id');

            $table->foreign('plante_id')->references('id')->on('plante');
            $table->foreign('propriete_plante_id')->references('id')->on('propriete_plante');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plante_propriete_plante');
    }
}
